class ParametersReader {
    getSearchParameters = () => {
        var prmstr = window.location.search.substr(1);
        return prmstr != null && prmstr != "" ? this.transformToAssocArray(prmstr) : {};
    }

    transformToAssocArray = ( prmstr ) => {
        let params = {};
        const prmarr = prmstr.split("&");
        for ( var i = 0; i < prmarr.length; i++) {
            var tmparr = prmarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }

        return params;
    }

    overwritePalletParameters = () => {
        let getParams = this.getSearchParameters();
        if (getParams.width) {
            $('#pallet-width').val(getParams.width);
        }

        if (getParams.length) {
            $('#pallet-length').val(getParams.length);
        }

        if (getParams.topBoards) {
            $('#pallet-top-boards').val(getParams.topBoards);
        }

        if (getParams.transverseBoards) {
            $('#pallet-transverse-boards').val(getParams.transverseBoards);
        }

        if (getParams.desk) {
            $('#desk-properties option[value=\'' + getParams.desk + '\']').attr('selected', 'selected');
        }
    }
}

