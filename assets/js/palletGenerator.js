class PalletGenerator {
    updateValuesOfObject = () => {
        this.pallet = {
            width: $('#pallet-width').val(),
            length: $('#pallet-length').val(),
            numberOfHorizontalBoards: $('#pallet-top-boards').val(),
            numberOfVerticalBoards: $('#pallet-transverse-boards').val(),
            baseBoardNumber: $('#pallet-transverse-boards').val(),
        };

        let desk = $('#desk-properties').val();
        switch (desk) {
            case '16x75':
                this.pallet.widthOfVerticalBoard = 75;
                this.pallet.widthOfHorizontalBoard = 75;
                this.pallet.baseBlockHeight = 75;
                this.pallet.baseBoardWidth = 75;
                this.pallet.boardThickness = 16;
                break;
            case '19x75':
                this.pallet.widthOfVerticalBoard = 75;
                this.pallet.widthOfHorizontalBoard = 75;
                this.pallet.baseBlockHeight = 75;
                this.pallet.baseBoardWidth = 75;
                this.pallet.boardThickness = 19;
                break;
            case '19x100':
                this.pallet.widthOfVerticalBoard = 100;
                this.pallet.widthOfHorizontalBoard = 100;
                this.pallet.baseBlockHeight = 80;
                this.pallet.baseBoardWidth = 100;
                this.pallet.boardThickness = 19;
                break;
            case '22x100':
                this.pallet.widthOfVerticalBoard = 100;
                this.pallet.widthOfHorizontalBoard = 100;
                this.pallet.baseBlockHeight = 80;
                this.pallet.baseBoardWidth = 100;
                this.pallet.boardThickness = 22;
                break;
            default:
                this.pallet.widthOfVerticalBoard = $('#pallet-top-board-width').val();
                this.pallet.widthOfHorizontalBoard = $('#pallet-transverse-board-width').val();
                this.pallet.baseBlockHeight = $('#base-block-height').val();
                this.pallet.baseBoardWidth = $('#base-board-width').val();
                this.pallet.boardThickness = $('#board-thickness').val();
                this.pallet.baseBoardNumber = $('#base-boards-number').val();
                break;
        }
    };

    generateBoards = (boardsContainerId, numberOfBoards, boardSize, palletSize, typeOfGrid) => {
        $(boardsContainerId).text('');

        let columnsStyle = '';
        for (let i = 1; i <= numberOfBoards; i++) {
            if (i !== 1) {
                $(boardsContainerId).append('<div class="break"></div>');
                columnsStyle = columnsStyle + this.returnBreakSize(palletSize, boardSize, numberOfBoards) + '% ';
            }
            $(boardsContainerId).append('<div class="board"></div>');

            //solving problem with periodic numbers that sum of grid wasn't 100% and there was 1px too less
            if (i !== (numberOfBoards)) {
                columnsStyle = columnsStyle + ((boardSize / palletSize) * 100) + '% ';
            } else {
                columnsStyle = columnsStyle + 'auto';
            }
        }

        let style = '';
        if (typeOfGrid === 'column') {
            style = {'grid-template-columns': columnsStyle};
        } else if(typeOfGrid === 'row') {
            style = {'grid-template-rows': columnsStyle};
        }
        $(boardsContainerId).css(style);
    };

    returnBreakSize = (width, boardWidth, numberOfBoards) => {
        return ((((width - (boardWidth * numberOfBoards)) / (numberOfBoards - 1)) / width) * 100);
    };

     resizePalletContainer = () => {
         let isFullWidth = ($( 'body' ).width() > 1140);
         let maxSizeOnSiteInPx = isFullWidth ? 600 : ($('body').width() * 0.8 - 100);

         let width  = this.pallet.width;
         let length = this.pallet.length;
         let scale = 1;
         const scaleRatio = 1.1;
         while (width > maxSizeOnSiteInPx) {
             width = width / scaleRatio;
             length = length / scaleRatio;
             scale = scale / scaleRatio;
         }

        let palletTotalHeight = Math.floor(((3 * this.pallet.boardThickness) + (this.pallet.baseBlockHeight * 1)) * scale) + 'px';

        $('#pallet-top-container > .grid-container').css({'width': Math.floor(width), 'height': Math.floor(length)});
        $('#pallet-top-height-info').css({'padding-top': Math.floor(length / 2) + 'px'});
        $('#pallet-front-container > .grid-container').css({'width': palletTotalHeight, 'height': Math.floor(length)});
        $('#pallet-top-width-info').css({'padding-left': (width / 2 - 24) + 'px'});
        $('#pallet-side-container > .grid-container').css({'width': width + 'px', 'height': palletTotalHeight});


        if (isFullWidth) {
            $('#pallet-view').css({'grid-template-columns': (width + 100) + 'px auto', 'grid-template-rows': (length + 40) + 'px auto'});
        } else {
            $('#pallet-view').css({'grid-template-columns': (width + 60) + 'px auto', 'grid-template-rows': (length + 50) + 'px auto'});
        }
    };

    setProportionsOfFrontAndSideViews = () => {
        let sumHeight = this.pallet.boardThickness * 3 + this.pallet.baseBlockHeight * 1;
        let rowsStyle = (this.pallet.boardThickness / sumHeight * 100) + '% ' +
            (this.pallet.boardThickness / sumHeight * 100) + '% ' +
            (this.pallet.baseBlockHeight / sumHeight * 100) + '% ' +
            'auto';

        $('#pallet-front-container > .grid-container').css({'grid-template-columns': rowsStyle});
        $('#pallet-side-container > .grid-container').css({'grid-template-rows': rowsStyle});
    };

    addInfoAboutWidthAndLength = () => {
        $('#pallet-top-width-info').text(this.pallet.width + ' mm');
        $('#pallet-top-height-info').text(this.pallet.length + ' mm');
        $('#pallet-front-height-info').text(this.pallet.boardThickness * 3 + this.pallet.baseBlockHeight * 1 + ' mm');
    };


    refreshOnInputChange = () => {
        let validator = new PalletDataValidator();

        this.updateValuesOfObject();
        if (validator.validate(this.pallet)) {
            this.resizePalletContainer();
            this.generateBoards('#pallet-top-top-boards', this.pallet.numberOfVerticalBoards, this.pallet.widthOfVerticalBoard, this.pallet.width, 'column');
            this.generateBoards('#pallet-top-transverse-boards', this.pallet.numberOfHorizontalBoards, this.pallet.widthOfHorizontalBoard, this.pallet.length, 'row');
            this.generateBoards('#pallet-top-bottom-boards', this.pallet.baseBoardNumber, this.pallet.baseBoardWidth, this.pallet.length, 'row');

            this.generateBoards('#pallet-front-top-boards', this.pallet.numberOfHorizontalBoards, this.pallet.widthOfHorizontalBoard, this.pallet.length, 'row');
            this.generateBoards('#pallet-front-base-block-height', this.pallet.baseBoardNumber, this.pallet.baseBoardWidth, this.pallet.length, 'row');
            this.generateBoards('#pallet-front-base-board', this.pallet.baseBoardNumber, this.pallet.baseBoardWidth, this.pallet.length, 'row');

            this.generateBoards('#pallet-side-transverse-boards', this.pallet.numberOfVerticalBoards, this.pallet.widthOfVerticalBoard, this.pallet.width, 'column');
            this.generateBoards('#pallet-side-blocks', this.pallet.numberOfVerticalBoards, this.pallet.widthOfVerticalBoard, this.pallet.width, 'column');

            this.setProportionsOfFrontAndSideViews();
            this.addInfoAboutWidthAndLength();
        }
    };
}