class PalletDataValidator
{
    validate = (pallet) => {
        this.pallet = pallet;
        this.allInputs = $("#inputs-container > input");

        if (!this.areAllInputsFilled()) {
            $('#inputs-errors').text('Nie wszystkie pola formularza zostały wypełnione');
            return false;
        }
        if (!this.isNumberOfBoardsInt()) {
            $('#inputs-errors').text('Liczba desek nie jest liczbą całkowitą');
            return false;
        }
        if (!this.areAllNumbersGreaterThan0()) {
            $('#inputs-errors').text('Nie wszystkie liczby są większe od 0');
            return false;
        }
        if (!this.doPalletsHaveAtLeast2Boards()) {
            $('#inputs-errors').text('Paleta musi mieć co najmniej 2 deski');
            return false;
        }
        if (!this.arentBoardsWiderThatPallet()) {
            $('#inputs-errors').text('Suma długości desek jest większa niż długość palety');
            return false;
        }
        if (!this.areWidthAndHeightCorrect()) {
            $('#inputs-errors').text('Paleta nie może być dłuższa niż 5000mm i szersza niż 2400mm');
            return false;
        }
        if (!this.areBottomBlocksLessWideThanPallet()) {
            $('#inputs-errors').text('Łaczna szerokość klocków spodnich jest większa niż szerokość palety');
            return false;
        }

        $('#inputs-errors').text('');

        return true;
    };

    areAllInputsFilled = () => {
        return (this.allInputs.filter(function () { return $.trim($(this).val()).length === 0}).length === 0);
    };

    arentBoardsWiderThatPallet = () => {
        return (this.pallet.width >= this.pallet.numberOfVerticalBoards * this.pallet.widthOfVerticalBoard &&
            this.pallet.length >= this.pallet.numberOfHorizontalBoards * this.pallet.widthOfHorizontalBoard);
    };

    doPalletsHaveAtLeast2Boards = () => {
        return (this.pallet.numberOfHorizontalBoards >= 2 &&
            this.pallet.numberOfVerticalBoards >= 2 &&
            this.pallet.baseBoardNumber >= 2);
    };

    isNumberOfBoardsInt = () => {
        return (Math.floor(this.pallet.numberOfHorizontalBoards) == this.pallet.numberOfHorizontalBoards &&
        Math.floor(this.pallet.numberOfVerticalBoards) == this.pallet.numberOfVerticalBoards);
    };

    areAllNumbersGreaterThan0 = () => {
        return (this.pallet.width > 0 &&
        this.pallet.length > 0 &&
        this.pallet.numberOfHorizontalBoards > 0 &&
        this.pallet.numberOfVerticalBoards > 0 &&
        this.pallet.widthOfVerticalBoard > 0 &&
        this.pallet.widthOfHorizontalBoard > 0);
    };

    areWidthAndHeightCorrect = () => {
        const maxLength = 2400;
        const maxWidth = 5000;

        return (this.pallet.length <= maxLength &&
        this.pallet.width <= maxWidth);
    };

    areBottomBlocksLessWideThanPallet = () => {
        return (this.pallet.width >= (this.pallet.baseBoardNumber * this.pallet.baseBoardWidth));
    }
}