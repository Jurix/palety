<!DOCTYPE html>
<html lang="pl">
<head style="user-select: auto;">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="INDEX,FOLLOW" style="user-select: auto;">
    <meta name="title" content="Zamówienie - Kampol Sklep internetowy" style="user-select: auto;">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" style="user-select: auto;">
    <meta name="format-detection" content="telephone=no" style="user-select: auto;">
    <title style="user-select: auto;">Zamówienie - Kampol Sklep internetowy</title>
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/calendar.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/styles-m.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/payu.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/megamenu.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/megamenu-theme.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="assets/css/site/styles-l.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" media="print" href="assets/css/site/print.css" style="user-select: auto;">
    <link href="assets/css/site/css.css" rel="stylesheet" type="text/css" style="user-select: auto;">
    <style type="text/css" style="user-select: auto;">.video-ads{display:none!important}#player-ads{display:none!important}#watch7-sidebar-ads{display:none!important}#AdSense{display:none!important}#homepage-sidebar-ads{display:none!important}#page-container>#page>#header{display:none!important}#content #page-manager #masthead-ad{display:none!important}#body-container #page-container #video-masthead-iframe{display:none!important}#feedmodule-PRO{display:none!important}#homepage-chrome-side-promo{display:none!important}#search-pva{display:none!important}#watch-branded-actions{display:none!important}#watch-buy-urls{display:none!important}.carousel-offer-url-container{display:none!important}.promoted-videos{display:none!important}.watch-extra-info-column{display:none!important}.watch-extra-info-right{display:none!important}a[href^="http://www.youtube.com/cthru?"]{display:none!important}</style>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/validator.js"></script>
    <script src="assets/js/palletGenerator.js"></script>
    <script src="assets/js/parametersReader.js"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WQ8M5X8');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '760522797687693');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=760522797687693&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body data-container="body" class="boxed-layout  checkout-index-index page-layout-checkout" style="user-select: auto;">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQ8M5X8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<link rel="stylesheet" href="assets/css/site/skin_brittany_yellow.css" style="user-select: auto;">
<div class="page-wrapper" style="user-select: auto;">
    <header class="page-header" style="user-select: auto;">
        <div class="header content checkout-header" style="user-select: auto;">
            <div class="middle-block" style="user-select: auto;">
                <div class="container" style="user-select: auto;">
                    <div class="middle-block-inner clearfix" style="user-select: auto;">
                        <div class="left-block" style="user-select: auto;">
                            <a class="logo" href="/" title="Kampol Logistyka Sklep internetowy" style="user-select: auto;">
                                <img src="assets/css/site/kampol_sklep_logo_1.png" alt="Kampol Logistyka Sklep internetowy" width="223" height="50" style="user-select: auto;">
                            </a>
                        </div>
                        <div class="hidden-xs right-block" style="user-select: auto;">
                            <div class="options-wrapper contacts-block" style="user-select: auto;">
                                <i class="meigee-support" style="user-select: auto;"></i>
                                <div style="user-select: auto;">
                                    <span class="title" style="user-select: auto;">Kontakt z konsultantem</span>
                                    <span class="phone" style="user-select: auto;"><a href="tel:+48717915995" style="user-select: auto;">(71) 791-59-95</a></span>
                                    <span class="phone" style="user-select: auto;"><a href="tel:+48517955109" style="user-select: auto;">517-955-109</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main id="maincontent" class="page-main" style="user-select: auto;">
        <a id="contentarea" tabindex="-1" style="user-select: auto;"></a>
        <div class="page-title-wrapper" style="user-select: auto;">
            <h1 class="page-title" style="user-select: auto;">
                <span class="base" data-ui-id="page-title-wrapper" style="user-select: auto;">Konfigurator palet</span>
            </h1>
        </div>
        <div class="columns" style="user-select: auto;">
            <div class="column main" style="user-select: auto;">
                <div id="container" data-bind="scope:&#39;checkout&#39;" class="checkout-container" >
                    <div id="inputs-container">
                        <div id="inputs-errors"></div>
                        <form method="POST" action="client.php">
                            Długość palety <input type="number" value="1200" id="pallet-width" name="width" required> mm<br>
                            Szerokość palety <input type="number" value="800" id="pallet-length" name="length" required> mm<br>
                            <br>
                            Ilość desek wierzchnich <input type="number" value="7" id="pallet-top-boards" name="topBoards" required><br>
                            Ilość desek poprzecznych <input type="number" value="3" id="pallet-transverse-boards" name="transverseBoards" required><br>
                            <br>
                            Wybór deski (grubość x szerokość)<br>
                            <select id="desk-properties" name="desk">
                                <option value="16x75">16 mm x 75 mm</option>
                                <option value="19x75">19 mm x 75 mm</option>
                                <option value="19x100">19 mm x 100 mm</option>
                                <option value="22x100">22 mm x 100 mm</option>
                            </select>
                            <input type="submit" value="Zatwierdź paletę" class="send-pallet btn btn-default">
                        </form>
                    </div>
                    <div id="pallet-view">
                        <div id="pallet-top-container">
                            <div id="pallet-top-width-info">

                            </div>
                            <div id="pallet-top-height-info">

                            </div>
                            <div class="grid-container">
                                <div id="pallet-top-top-boards">

                                </div>
                                <div id="pallet-top-transverse-boards">

                                </div>

                                <div id="pallet-top-bottom-boards">

                                </div>
                                <div class="img-container">
                                    <img src="assets/img/logo.png">
                                </div>
                            </div>
                        </div>

                        <div id="pallet-front-container">
                            <div id="pallet-front-height-info">

                            </div>
                            <div class="grid-container">
                                <div id="pallet-front-top-boards">

                                </div>
                                <div id="pallet-front-transverse-boards">
                                    <div class="board"></div>
                                </div>
                                <div id="pallet-front-base-block-height">

                                </div>
                                <div id="pallet-front-base-board">

                                </div>
                                <div class="img-container">
                                    <img src="assets/img/logo-vert.png">
                                </div>
                            </div>
                        </div>

                        <div id="pallet-side-container">
                            <div>

                            </div>
                            <div class="grid-container">
                                <div class="pallet-side-top-boards">
                                    <div class="board"></div>
                                </div>
                                <div id="pallet-side-transverse-boards">

                                </div>
                                <div id="pallet-side-blocks">

                                </div>
                                <div class="pallet-side-bottom-boards">
                                    <div class="board"></div>
                                </div>
                                <div class="img-container">
                                    <img src="assets/img/logo.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="page-footer">
        <div class="footer-container">
            <div class="footer accordion-list">
                <div class="footer-middle">
                    <div class="container clearfix">
                        <hr class="indent-32 white-space hidden-sm hidden-xs">
                        <div class="row">
                            <div class="col-md-4 col-lg-3 accordion-item open">
                                <div class="accordion-content" style="">
                                    <div class="footer-logo">
                                        <a href="/" title="Kampol Sklep Internetowy"><img src="assets/img/kampol_sklep_logo.png" alt="Kampol Sklep Internetowy"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-3 accordion-item">
                                <div class="accordion-content">
                                    <p><b>KAMPOL LOGISTYKA Sp. z o.o.</b><br>Źródła, ul. Maszynowa 9<br>55-330 Miękinia</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-3 accordion-item">
                                <div class="accordion-content">
                                    <div class="company-contact-wrapper">
                                        <i class="meigee-email"></i>
                                        <div>
                                            <span class="email"><a href="mailto:sklep@kampol-logistyka.pl">sklep@kampol-logistyka.pl</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-3 accordion-item">
                                <div class="accordion-content">
                                    <div class="company-contact-wrapper">
                                        <i class="meigee-phone"></i>
                                        <div>
                                            <span class="phone"><a href="tel:+48717915995">(71) 791-59-95</a></span><br><span class="phone"><a href="tel:+48517955109">517-955-109</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="indent-60 white-space hidden-xs hidden-sm">
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container clearfix">
                        <div class="copyright-wrapper">
                            <address><span class="copyright">© 2011-2020 KAMPOL LOGISTYKA Sp. z o.o. Wszystkie prawa zastrzeżone. Autor strony: <a title="Yasecure Łukasz Podlewski tworzenie stron www i sklepów internetowych" href="https://yasecure.pl">Yasecure tworzenie stron www</a></span></address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <link rel="stylesheet" type="text/css" media="all" href="assets/css/site/yasecure.css" style="user-select: auto;">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="assets/js/script.js"></script>
</div>
</body>
</html>