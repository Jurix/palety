<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MessageSender
{
    private $host = 'kampol-logistyka.pl';
    private $username = 'sklep@kampol-logistyka.pl';
    private $password = '90n{{38D';

    private $body;
    private $title;
    private $mailer;
    private $bodyOperator;

    public function __construct($message, $mailer)
    {
        $this->body = $message->getBody();
        $this->bodyOperator = $message->getBodyOperator();
        $this->title = $message->getTitle();
        $this->mailer = $mailer;
    }

    private function connectToServer()
    {
        try {
            $mail = $this->mailer;

            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            //$mail->SMTPDebug = 2;
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = $this->host;                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = $this->username;                     // SMTP username
            $mail->Password = $this->password;                               // SMTP password
            $mail->SMTPAutoTLS = false;
            $mail->CharSet = 'UTF-8';
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted  ENCRYPTION_STARTTLS
            $mail->Port = 587;                                    // TCP port to connect to

            $this->mailer = $mail;

            return $mail;
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    public function sendMessage()
    {
        $mail = $this->connectToServer();
        try {
            //Recipients
            $mail->setFrom('sklep@kampol-logistyka.pl', 'Sklep Kampol Logistyka');
            $mail->addAddress($_POST['email']);
            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $this->title;
            $mail->Body = $this->body;

            $mail->send();

            $mail->ClearAddresses();
            $mail->Body = $this->bodyOperator;
            $mail->addAddress($this->username);
            $mail->AddReplyTo($_POST['email'], $_POST['firstname'].' '.$_POST['surname']);

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}