<?php

class MessageGetter
{
    private $palletGeneratorUrl = 'https://sklep.kampol-logistyka.pl/konfigurator/';

    public function getTitle()
    {
        return 'Konfiguracja palety ' . $this->returnPalletSize();
    }

    public function getBody()
    {
        $messageClient = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-size: 62.5%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-size-adjust: 100%; background-color: #f5f5f5;">
<head>
    <style type="text/css.css">
        url("https://sklep.kampol-logistyka.pl/pub/static/version1579707510/frontend/Yasecure/coccinelle_child/en_US/css.css/email-fonts.css.css");html{font-size:62.5%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;font-size-adjust:100%}body{color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:normal;font-weight:400;line-height:1.42857143;font-size:14px}p{margin-top:0;margin-bottom:10px}abbr[title]{border-bottom:1px dotted #ccc;cursor:help}b,strong{font-weight:700}em,i{font-style:italic}mark{background:#f6f6f6;color:#000}small,.small{font-size:12px}hr{border:0;border-top:1px solid #ccc;margin-bottom:20px;margin-top:20px}sub,sup{font-size:71.42857143000001%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}dfn{font-style:italic}h1{font-weight:300;line-height:1.1;font-size:26px;margin-top:0;margin-bottom:20px}h2{font-weight:300;line-height:1.1;font-size:26px;margin-top:25px;margin-bottom:20px}h3{font-weight:300;line-height:1.1;font-size:18px;margin-top:20px;margin-bottom:10px}h4{font-weight:700;line-height:1.1;font-size:14px;margin-top:20px;margin-bottom:20px}h5{font-weight:700;line-height:1.1;font-size:12px;margin-top:20px;margin-bottom:20px}h6{font-weight:700;line-height:1.1;font-size:10px;margin-top:20px;margin-bottom:20px}h1 small,h2 small,h3 small,h4 small,h5 small,h6 small,h1 .small,h2 .small,h3 .small,h4 .small,h5 .small,h6 .small{color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:normal;font-weight:400;line-height:1}a,.alink{color:#006bb4;text-decoration:none}a:visited,.alink:visited{color:#006bb4;text-decoration:none}a:hover,.alink:hover{color:#006bb4;text-decoration:underline}a:active,.alink:active{color:#ff5501;text-decoration:underline}ul,ol{margin-top:0;margin-bottom:25px}ul>li,ol>li{margin-top:0;margin-bottom:10px}ul ul,ol ul,ul ol,ol ol{margin-bottom:0}dl{margin-bottom:20px;margin-top:0}dt{font-weight:700;margin-bottom:5px;margin-top:0}dd{margin-bottom:10px;margin-top:0;margin-left:0}code,kbd,pre,samp{font-family:Menlo,Monaco,Consolas,\'Courier New\',monospace}code{background:#f6f6f6;color:#111;padding:2px 4px;font-size:12px;white-space:nowrap}kbd{background:#f6f6f6;color:#111;padding:2px 4px;font-size:12px}pre{background:#f6f6f6;border:1px solid #ccc;color:#111;line-height:1.42857143;margin:0 0 10px;padding:10px;font-size:12px;display:block;word-wrap:break-word}pre code{background-color:transparent;border-radius:0;color:inherit;font-size:inherit;padding:0;white-space:pre-wrap}blockquote{border-left:0 solid #ccc;margin:0 0 20px 40px;padding:0;color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:italic;font-weight:400;line-height:1.42857143;font-size:14px}blockquote p:last-child,blockquote ul:last-child,blockquote ol:last-child{margin-bottom:0}blockquote footer,blockquote small,blockquote .small{color:#333;line-height:1.42857143;font-size:10px;display:block}blockquote footer:before,blockquote small:before,blockquote .small:before{content:\'\2014 \00A0\'}blockquote cite{font-style:normal}blockquote:before,blockquote:after{content:\'\'}q{quotes:none}q:before,q:after{content:\'\';content:none}cite{font-style:normal}.shipment-track th{text-align:left}.shipment-track>tbody>tr>th,.shipment-track>tfoot>tr>th,.shipment-track>tbody>tr>td,.shipment-track>tfoot>tr>td{vertical-align:top}.shipment-track>thead>tr>th,.shipment-track>thead>tr>td{vertical-align:bottom}.shipment-track>thead>tr>th,.shipment-track>tbody>tr>th,.shipment-track>tfoot>tr>th,.shipment-track>thead>tr>td,.shipment-track>tbody>tr>td,.shipment-track>tfoot>tr>td{padding:0 10px}.email-items th{text-align:left}.email-items>tbody>tr>th,.email-items>tfoot>tr>th,.email-items>tbody>tr>td,.email-items>tfoot>tr>td{vertical-align:top}.email-items>thead>tr>th,.email-items>thead>tr>td{vertical-align:bottom}.email-items>thead>tr>th,.email-items>tbody>tr>th,.email-items>tfoot>tr>th,.email-items>thead>tr>td,.email-items>tbody>tr>td,.email-items>tfoot>tr>td{padding:0 10px}@media only screen and (max-width:639px){html,body{background-color:#fff;width:100% !important}.main{max-width:100% !important;min-width:240px;width:auto !important}.rma-items td,.rma-items th{font-size:12px !important;padding:5px !important}}@media only screen and (max-width:479px){.header,.main-content,.footer{padding:25px 10px !important}.footer td{display:block;width:auto !important}.email-features>tbody>tr>td{clear:both;display:block;padding-top:20px;width:auto !important}.email-summary h1{font-size:24px !important}.order-details .address-details,.order-details .method-info{display:block;padding:10px 0 !important;width:auto !important}.order-details .address-details h3,.order-details .method-info h3{margin-bottom:5px !important;margin-top:0 !important}.button .inner-wrapper{width:100% !important}.button .inner-wrapper td a{font-size:16px}}body,table,td,a{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}img{-ms-interpolation-mode:bicubic}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}a:visited{color:#006bb4 !important;text-decoration:none !important}a:hover{color:#006bb4 !important;text-decoration:underline !important}a:active{color:#ff5501 !important;text-decoration:underline !important}.no-link a,.address-details a{color:#333 !important;cursor:default !important;text-decoration:none !important}.button .inner-wrapper td:hover{background-color:#006bb4 !important}.button .inner-wrapper a:active,.button .inner-wrapper td:active{background-color:#006bb4 !important}.button a:active,.button a:hover,.button a:visited{border:1px solid #006bb4;color:#fff !important;text-decoration:none !important}.email-items{overflow-x:auto;overflow-y:hidden;width:100%;-ms-overflow-style:-ms-autohiding-scrollbar;-webkit-overflow-scrolling:touch}
    </style>
    <style type="text/css.css">a:visited{color:#006bb4;text-decoration:none}a:hover{color:#006bb4;text-decoration:underline}a:active{color:#ff5501;text-decoration:underline}</style>
</head>
<body style="margin: 0; padding: 0; color: #333; font-style: normal; line-height: 1.42857143; font-size: 14px; font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; font-weight: normal; text-align: left; background-color: #f5f5f5;">
<!-- Begin wrapper table -->
<table class="wrapper" width="100%" style="border-collapse: collapse; margin: 0 auto;">
    <tr>
        <td class="wrapper-inner" align="center" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 30px; width: 100%;">
            <table class="main" align="center" style="border-collapse: collapse; margin: 0 auto; text-align: left; width: 660px;">
                <tr>
                    <td class="header" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #f5f5f5; padding: 25px;">
                        <a class="logo" href="https://sklep.kampol-logistyka.pl/" style="color: #006bb4; text-decoration: none;">
                            <img width="180" src="https://sklep.kampol-logistyka.pl/pub/media/email/logo/default/kampol_sklep_logo.png" alt="Kampol Logistyka" border="0" style="border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;"></a>
                    </td>
                </tr>
                <tr>
                    <td class="main-content" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #fff; padding: 25px;">
                        <div>
                            Dzień dobry,<br>
                            Stworzona przez Ciebie paleta: <a href="' . $this->returnPalletGeneratorWithParams() . '">' . $this->returnPalletGeneratorWithParams() . '</a><br><br>
                            ' . $this->returnTextPalletData() . '<br><br>';
        if (!empty($_POST['comments'])) {
            $messageClient .= 'Twoje uwagi:<br>'.nl2br($_POST['comments']).'<br><br>';
        }

        $messageClient .= '<p>Konsultant w ciągu 24 godzin roboczych zapozna się z projektem i skontaktuje się z Państwem celem przedstawienia wyceny</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="footer" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #f5f5f5; padding: 25px;">
                        <table style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                </td>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                    <p class="phone" style="margin-top: 0; margin-bottom: 0; font-size: 18px;">
                                        <a href="tel:+48%2071%20314%2042%2059" style="text-decoration: none; color: inherit;"> +48 71 314 42 59</a>
                                    </p>
                                    <p class="hours" style="margin-top: 0; margin-bottom: 0;">
                                        Godziny pracy: <span class="no-link">Pn - Pt, 08:00 - 16:00</span>.
                                    </p>
                                </td>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                    <p class="address" style="margin-top: 0; margin-bottom: 0;">
                                        Kampol Logistyka<br>
                                        Źródła, ul. Maszynowa 9<br>
                                        Miękinia,  55-330,<br>
                                        Poland
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>';

        return $messageClient;
    }

    public function getBodyOperator()
    {
        $messageOperator = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-size: 62.5%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-size-adjust: 100%; background-color: #f5f5f5;">
<head>
    <style type="text/css.css">
        url("https://sklep.kampol-logistyka.pl/pub/static/version1579707510/frontend/Yasecure/coccinelle_child/en_US/css.css/email-fonts.css.css");html{font-size:62.5%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;font-size-adjust:100%}body{color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:normal;font-weight:400;line-height:1.42857143;font-size:14px}p{margin-top:0;margin-bottom:10px}abbr[title]{border-bottom:1px dotted #ccc;cursor:help}b,strong{font-weight:700}em,i{font-style:italic}mark{background:#f6f6f6;color:#000}small,.small{font-size:12px}hr{border:0;border-top:1px solid #ccc;margin-bottom:20px;margin-top:20px}sub,sup{font-size:71.42857143000001%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}dfn{font-style:italic}h1{font-weight:300;line-height:1.1;font-size:26px;margin-top:0;margin-bottom:20px}h2{font-weight:300;line-height:1.1;font-size:26px;margin-top:25px;margin-bottom:20px}h3{font-weight:300;line-height:1.1;font-size:18px;margin-top:20px;margin-bottom:10px}h4{font-weight:700;line-height:1.1;font-size:14px;margin-top:20px;margin-bottom:20px}h5{font-weight:700;line-height:1.1;font-size:12px;margin-top:20px;margin-bottom:20px}h6{font-weight:700;line-height:1.1;font-size:10px;margin-top:20px;margin-bottom:20px}h1 small,h2 small,h3 small,h4 small,h5 small,h6 small,h1 .small,h2 .small,h3 .small,h4 .small,h5 .small,h6 .small{color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:normal;font-weight:400;line-height:1}a,.alink{color:#006bb4;text-decoration:none}a:visited,.alink:visited{color:#006bb4;text-decoration:none}a:hover,.alink:hover{color:#006bb4;text-decoration:underline}a:active,.alink:active{color:#ff5501;text-decoration:underline}ul,ol{margin-top:0;margin-bottom:25px}ul>li,ol>li{margin-top:0;margin-bottom:10px}ul ul,ol ul,ul ol,ol ol{margin-bottom:0}dl{margin-bottom:20px;margin-top:0}dt{font-weight:700;margin-bottom:5px;margin-top:0}dd{margin-bottom:10px;margin-top:0;margin-left:0}code,kbd,pre,samp{font-family:Menlo,Monaco,Consolas,\'Courier New\',monospace}code{background:#f6f6f6;color:#111;padding:2px 4px;font-size:12px;white-space:nowrap}kbd{background:#f6f6f6;color:#111;padding:2px 4px;font-size:12px}pre{background:#f6f6f6;border:1px solid #ccc;color:#111;line-height:1.42857143;margin:0 0 10px;padding:10px;font-size:12px;display:block;word-wrap:break-word}pre code{background-color:transparent;border-radius:0;color:inherit;font-size:inherit;padding:0;white-space:pre-wrap}blockquote{border-left:0 solid #ccc;margin:0 0 20px 40px;padding:0;color:#333;font-family:\'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-style:italic;font-weight:400;line-height:1.42857143;font-size:14px}blockquote p:last-child,blockquote ul:last-child,blockquote ol:last-child{margin-bottom:0}blockquote footer,blockquote small,blockquote .small{color:#333;line-height:1.42857143;font-size:10px;display:block}blockquote footer:before,blockquote small:before,blockquote .small:before{content:\'\2014 \00A0\'}blockquote cite{font-style:normal}blockquote:before,blockquote:after{content:\'\'}q{quotes:none}q:before,q:after{content:\'\';content:none}cite{font-style:normal}.shipment-track th{text-align:left}.shipment-track>tbody>tr>th,.shipment-track>tfoot>tr>th,.shipment-track>tbody>tr>td,.shipment-track>tfoot>tr>td{vertical-align:top}.shipment-track>thead>tr>th,.shipment-track>thead>tr>td{vertical-align:bottom}.shipment-track>thead>tr>th,.shipment-track>tbody>tr>th,.shipment-track>tfoot>tr>th,.shipment-track>thead>tr>td,.shipment-track>tbody>tr>td,.shipment-track>tfoot>tr>td{padding:0 10px}.email-items th{text-align:left}.email-items>tbody>tr>th,.email-items>tfoot>tr>th,.email-items>tbody>tr>td,.email-items>tfoot>tr>td{vertical-align:top}.email-items>thead>tr>th,.email-items>thead>tr>td{vertical-align:bottom}.email-items>thead>tr>th,.email-items>tbody>tr>th,.email-items>tfoot>tr>th,.email-items>thead>tr>td,.email-items>tbody>tr>td,.email-items>tfoot>tr>td{padding:0 10px}@media only screen and (max-width:639px){html,body{background-color:#fff;width:100% !important}.main{max-width:100% !important;min-width:240px;width:auto !important}.rma-items td,.rma-items th{font-size:12px !important;padding:5px !important}}@media only screen and (max-width:479px){.header,.main-content,.footer{padding:25px 10px !important}.footer td{display:block;width:auto !important}.email-features>tbody>tr>td{clear:both;display:block;padding-top:20px;width:auto !important}.email-summary h1{font-size:24px !important}.order-details .address-details,.order-details .method-info{display:block;padding:10px 0 !important;width:auto !important}.order-details .address-details h3,.order-details .method-info h3{margin-bottom:5px !important;margin-top:0 !important}.button .inner-wrapper{width:100% !important}.button .inner-wrapper td a{font-size:16px}}body,table,td,a{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}img{-ms-interpolation-mode:bicubic}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt}a:visited{color:#006bb4 !important;text-decoration:none !important}a:hover{color:#006bb4 !important;text-decoration:underline !important}a:active{color:#ff5501 !important;text-decoration:underline !important}.no-link a,.address-details a{color:#333 !important;cursor:default !important;text-decoration:none !important}.button .inner-wrapper td:hover{background-color:#006bb4 !important}.button .inner-wrapper a:active,.button .inner-wrapper td:active{background-color:#006bb4 !important}.button a:active,.button a:hover,.button a:visited{border:1px solid #006bb4;color:#fff !important;text-decoration:none !important}.email-items{overflow-x:auto;overflow-y:hidden;width:100%;-ms-overflow-style:-ms-autohiding-scrollbar;-webkit-overflow-scrolling:touch}
    </style>
    <style type="text/css.css">a:visited{color:#006bb4;text-decoration:none}a:hover{color:#006bb4;text-decoration:underline}a:active{color:#ff5501;text-decoration:underline}</style>
</head>
<body style="margin: 0; padding: 0; color: #333; font-style: normal; line-height: 1.42857143; font-size: 14px; font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; font-weight: normal; text-align: left; background-color: #f5f5f5;">
<!-- Begin wrapper table -->
<table class="wrapper" width="100%" style="border-collapse: collapse; margin: 0 auto;">
    <tr>
        <td class="wrapper-inner" align="center" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 30px; width: 100%;">
            <table class="main" align="center" style="border-collapse: collapse; margin: 0 auto; text-align: left; width: 660px;">
                <tr>
                    <td class="header" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #f5f5f5; padding: 25px;">
                        <a class="logo" href="https://sklep.kampol-logistyka.pl/" style="color: #006bb4; text-decoration: none;">
                            <img width="180" src="https://sklep.kampol-logistyka.pl/pub/media/email/logo/default/kampol_sklep_logo.png" alt="Kampol Logistyka" border="0" style="border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;"></a>
                    </td>
                </tr>
                <tr>
                    <td class="main-content" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #fff; padding: 25px;">
                        <div>
                            Klient: ' . $_POST['firstname'] . ' ' . $_POST['surname'] . '<br>
                            Nr telefonu: <a href="tel:' . $_POST['tel'] . '">' . $_POST['tel'] . '</a><br>
                            Email: ' . $_POST['email'] . '<br>
                            Paleta stworzona przez klienta: <a href="' . $this->returnPalletGeneratorWithParams() . '">' . $this->returnPalletGeneratorWithParams() . '</a><br><br>
                            ' . $this->returnTextPalletData() . '<br><br>
                            Uwagi:<br>
                            ' . nl2br($_POST['comments']) . '<br><br>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="footer" style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; background-color: #f5f5f5; padding: 25px;">
                        <table style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                </td>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                    <p class="phone" style="margin-top: 0; margin-bottom: 0; font-size: 18px;">
                                        <a href="tel:+48%2071%20314%2042%2059" style="text-decoration: none; color: inherit;"> +48 71 314 42 59</a>
                                    </p>
                                    <p class="hours" style="margin-top: 0; margin-bottom: 0;">
                                        Godziny pracy: <span class="no-link">Pn - Pt, 08:00 - 16:00</span>.
                                    </p>
                                </td>
                                <td style="font-family: \'Open Sans\',\'Helvetica Neue\',Helvetica,Arial,sans-serif; vertical-align: top; padding-bottom: 25px; width: 33%;">
                                    <p class="address" style="margin-top: 0; margin-bottom: 0;">
                                        Kampol Logistyka<br>
                                        Źródła, ul. Maszynowa 9<br>
                                        Miękinia,  55-330,<br>
                                        Poland
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>';

        return $messageOperator;
    }

    private function returnPalletSize()
    {
        $pallet = json_decode($_POST['palletData']);
        return $pallet->width . 'x' . $pallet->length;
    }

    private function returnPalletGeneratorWithParams()
    {
        return $this->palletGeneratorUrl . '?' . http_build_query(json_decode($_POST['palletData']));
    }

    private function returnTextPalletData()
    {
        $pallet = json_decode($_POST['palletData']);
        return ('Parametry palety:<br>
Długość palety: ' . $pallet->width . '<br>
Szerokość palety: ' . $pallet->length . '<br>
Ilość desek wierzchnich: ' . $pallet->topBoards . '<br>
Ilość desek spodnich: ' . $pallet->transverseBoards . '<br>
Wybrany typ deski: ' . $pallet->desk);
    }
}